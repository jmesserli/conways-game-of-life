/*
 # Game Of Life Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see [http://www.gnu.org/licenses/].
 #################################################################################################*/

package ch.jmnetwork.gameoflife.settings;

public class GOLSettings {

    public final String ALIVE_CELL_SYMBOL, DEAD_CELL_SYMBOL;
    public final int SIZE;

    public GOLSettings(String ALIVE_CELL_SYMBOL, String DEAD_CELL_SYMBOL, int SIZE) {
        this.ALIVE_CELL_SYMBOL = ALIVE_CELL_SYMBOL;
        this.DEAD_CELL_SYMBOL = DEAD_CELL_SYMBOL;
        this.SIZE = SIZE;
    }
}
