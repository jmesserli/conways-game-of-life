/*
 # Game Of Life Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see [http://www.gnu.org/licenses/].
 #################################################################################################*/

package ch.jmnetwork.gameoflife.game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Structure implements Iterable<Point> {

    private ArrayList<Point> activeCells = new ArrayList<>();

    public void setCellActive(Point p) {
        activeCells.add(p);
    }

    public void setCellsActive(ArrayList<Point> pointArrayList) {
        activeCells.addAll(pointArrayList);
    }

    public void setCellsActive(Point[] points) {
        Collections.addAll(activeCells, points);
    }

    public ArrayList<Point> getActiveCells() {
        return activeCells;
    }

    @Override
    public Iterator<Point> iterator() {
        return activeCells.iterator();
    }
}
