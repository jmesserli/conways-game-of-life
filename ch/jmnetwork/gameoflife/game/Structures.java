/*
 # Game Of Life Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see [http://www.gnu.org/licenses/].
 #################################################################################################*/

package ch.jmnetwork.gameoflife.game;

import java.awt.*;

/**
 * User: joel / Date: 26.01.14 / Time: 17:52
 */
public class Structures {

    public static Structure glider = new Structure();
    public static Structure line = new Structure();

    static {
        glider.setCellsActive(new Point[]{new Point(1, 3), new Point(2, 4), new Point(3, 2), new Point(3, 3), new Point(3, 4)});
        line.setCellsActive(new Point[]{new Point(5, 5), new Point(6, 5), new Point(7, 5)});
    }
}
