/*
 # Game Of Life Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see [http://www.gnu.org/licenses/].
 #################################################################################################*/

package ch.jmnetwork.gameoflife.game;

import ch.jmnetwork.core.tools.iterate.Runner;
import ch.jmnetwork.gameoflife.settings.GOLSettings;

import java.awt.*;
import java.io.PrintStream;

public class GameOfLife {
    private int FIELD_SIZE = 10;
    private String DEAD_CELL = "-";
    private String ALIVE_CELL = "\u00b0";
    private int[][] field = new int[FIELD_SIZE][FIELD_SIZE];
    private int[][] secondField = new int[FIELD_SIZE][FIELD_SIZE];

    public long countedNeighbors = 0L;

    public GameOfLife() {
        createEmptyField();
    }

    public void setSettings(GOLSettings settings) {
        FIELD_SIZE = settings.SIZE;
        DEAD_CELL = settings.DEAD_CELL_SYMBOL;
        ALIVE_CELL = settings.ALIVE_CELL_SYMBOL;

        field = new int[FIELD_SIZE][FIELD_SIZE];
        secondField = new int[FIELD_SIZE][FIELD_SIZE];
    }

    public void evolution(int count) {
        new Runner(count) {
            @Override
            protected void doRun() {
                evolution();
            }
        }.run();
    }

    /**
     * Step 1 lifecycle forward.
     */
    public void evolution() {
        createSecondField();
        for (int x = 0; x < FIELD_SIZE; x++) {
            for (int y = 0; y < FIELD_SIZE; y++) {
                int nbCount = countNeighbors(x, y);

                switch (nbCount) {
                    case 3:
                        secondField[x][y] = 1;
                        break;
                    case 2:
                        secondField[x][y] = field[x][y];
                        break;
                    default:
                        secondField[x][y] = 0;
                }
            }
        }

        copyArray();
    }

    /**
     * Counts the neighbors of the given coordinates.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @return Number of neighbors for the given coordinates
     */
    public int countNeighbors(int x, int y) {
        int neighborCount = 0;

        for (int a = -1; a < 2; a++) {
            for (int b = -1; b < 2; b++) {
                try {
                    if ((a != 0 || b != 0) && field[x + a][y + b] == 1) {
                        neighborCount++;
                        countedNeighbors++;
                    }
                } catch (Exception ignored) {
                    //GOLMain.l.log(LogLevel.INFO, String.format("Failed to count neighbor at [%d][%d]: %s", x + a, y + b, ignored));
                }
            }
        }

        return neighborCount;
    }

    /**
     * Creates an empty field with no living cells on it.
     */
    public void createEmptyField() {
        for (int a = 0; a < FIELD_SIZE; a++) {
            for (int b = 0; b < FIELD_SIZE; b++) {
                field[a][b] = 0;
            }// end for
        }// end for
    }

    /**
     * Creates the second field which is necessary for the evolution.
     */
    public void createSecondField() {
        for (int a = 0; a < FIELD_SIZE; a++) {
            for (int b = 0; b < FIELD_SIZE; b++) {
                secondField[a][b] = 0;
            }
        }
    }

    public void copyArray() {
        for (int a = 0; a < FIELD_SIZE; a++) {
            for (int b = 0; b < FIELD_SIZE; b++) {
                field[a][b] = secondField[a][b];
            }
        }
    }

    public void insertStructure(Structure s) {
        for (Point p : s) {
            field[p.x][p.y] = 1;
        }
    }

    /**
     * Generates a random field for the game.
     */
    public void generateRandomField() {
        for (int a = 0; a < FIELD_SIZE; a++) {
            for (int b = 0; b < FIELD_SIZE; b++) {
                field[a][b] = (int) Math.round(Math.random());
            }// end for
        }// end for
    }

    public void printField() {
        printField(System.out);
    }

    /**
     * Prints out the field of the game.
     */
    public void printField(PrintStream p) {
        for (int a = 0; a < FIELD_SIZE; a++) {
            for (int b = 0; b < FIELD_SIZE; b++) {
                if (field[a][b] == 0) {
                    p.print(" " + DEAD_CELL);
                } else {
                    p.print(" " + ALIVE_CELL);
                }
                if (b == FIELD_SIZE - 1) {
                    // last iteration of row
                    p.println("");
                }
            }
        }
        p.println("");
    }
}
