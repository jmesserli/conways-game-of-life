/*
 # Game Of Life Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see [http://www.gnu.org/licenses/].
 #################################################################################################*/

package ch.jmnetwork.gameoflife.run;

import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.logging.Logger;
import ch.jmnetwork.core.tools.iterate.Runner;
import ch.jmnetwork.gameoflife.game.GameOfLife;
import ch.jmnetwork.gameoflife.game.Structures;
import ch.jmnetwork.gameoflife.settings.GOLSettings;

import java.io.File;

public class GOLMain {

    public static Logger l = new Logger(new File("gol_log.log"), "GOL");

    public static void main(String[] args) {
        final GameOfLife game = new GameOfLife();
        game.setSettings(new GOLSettings("#", "-", 20));

        game.insertStructure(Structures.glider);

        new Runner(10) {
            @Override
            protected void doRun() {
                game.printField();
                game.evolution();
            }
        }.run();

        l.log(LogLevel.INFO, "Counted " + game.countedNeighbors + " neighbors.");
    }
}